library ieee;
use ieee.std_logic_1164.all;
entity APART is
port(
SpIn,SpOut: in std_logic_vector(2 downto 0) ;
Rin,Rout: in std_logic;
inTempReg: in std_logic_vector(0 downto 0);
Clk,Reset,memoryflag:IN std_logic ;
outMemory:IN std_logic_vector(31 downto 0);
outOfMar: out std_logic_vector (31 downto 0);
AluSelection : in std_logic_vector(4 downto 0);
outputFlag: out std_logic_vector (1 downto 0);
valid : in std_logic;
outOfMdr:out std_logic_vector (31 downto 0);
flagEnable:in std_logic ;
outOfIR:out std_logic_vector (31 downto 0)
);

end entity APART ;

Architecture APART of APART is 

COMPONENT busAndRegs is 
Port(
EnableReg:In std_logic_vector (11 downto 0); -- we have 10 reg but pc is enabled with 2 circuits
EnableOutOfReg:In std_logic_vector (9 downto 0); 
Clk,Reset,memoryflag:IN std_logic ;
outMemory:IN std_logic_vector(31 downto 0);
outOfMar: out std_logic_vector (31 downto 0);
AluSelection : in std_logic_vector(4 downto 0);
outputFlag: out std_logic_vector (1 downto 0);
outOfMdr:out std_logic_vector (31 downto 0);
outOfIR:out std_logic_vector (31 downto 0);
 BusB,BusC : Inout std_logic_vector (31 downto 0);
 flagEnable:in std_logic ;
valid : in std_logic

);
END COMPONENT;

COMPONENT decodersOfGrouping is 
port( SpIn,SpOut ,Ir9to7,Ir4to2: in std_logic_vector(2 downto 0) ;
Ir15to14: in std_logic_vector(1 downto 0) ;
SpInOut,SpOutOut,RiInOut,RiOutOut :out std_logic_vector (7 downto 0);
Rin,Rout: in std_logic;
inTempReg: in std_logic_vector(0 downto 0)
);
END COMPONENT;

SIGNAL SpInOutSig,SpOutOutSig,RiInOutSig,RiOutOutSig : std_logic_vector (7 downto 0);
SIGNAL EnableRegSig :std_logic_vector (11 downto 0);
SIGNAL outOfMarSig: std_logic_vector (31 downto 0);
SIGNAL outputFlagSignal: std_logic_vector (1 downto 0);
SIGNAL EnableOutOfRegSig :std_logic_vector (9 downto 0); 
SIGNAL outOfMdrSignal: std_logic_vector (31 downto 0);
SIGNAL outOfIRSignal:std_logic_vector (31 downto 0);
SIGNAL Ir9to7,Ir4to2: std_logic_vector(2 downto 0) ;
SIGNAL Ir15to14: std_logic_vector(1 downto 0) ;
SIGNAL BusB,BusC : std_logic_vector (31 downto 0);

begin


EnableRegSig<=(SpInOutSig(6)&SpInOutSig(4)&RiInOutSig(4)&SpInOutSig(1)&SpInOutSig(5)&SpInOutSig(7)&SpInOutSig(2)&SpInOutSig(3)&RiInOutSig(3)&RiInOutSig(2)&RiInOutSig(1)&RiInOutSig(0));

EnableOutOfRegSig<=(RiOutOutSig(4)&SpOutOutSig(1)&SpOutOutSig(4)&SpOutOutSig(6)&SpOutOutSig(2)&SpOutOutSig(3)&RiOutOutSig(3)&RiOutOutSig(2)&RiOutOutSig(1)&RiOutOutSig(0));

comp1: decodersOfGrouping  PORT MAP (SpIn,SpOut ,Ir9to7,Ir4to2,Ir15to14,SpInOutSig,SpOutOutSig,RiInOutSig,RiOutOutSig,Rin,Rout,inTempReg);
comp2: busAndRegs PORT MAP(EnableRegSig,EnableOutOfRegSig,Clk,Reset,memoryflag,outMemory,outOfMarSig,AluSelection,outputFlagSignal,outOfMdrSignal,outOfIRSignal,BusB,BusC,flagEnable,valid);
outOfMar<= outOfMarSig;
outputFlag<=outputFlagSignal;
outOfIR<=outOfIRSignal;
outOfMdr<=outOfMdrSignal;
Ir9to7<=outOfIRSignal(9 downto 7);
Ir4to2<=outOfIRSignal(4 downto 2);
Ir15to14<=outOfIRSignal(15 downto 14);
end APART ;