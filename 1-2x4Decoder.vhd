library ieee;
use ieee.std_logic_1164.all;

entity Decoder2x4 is
  port (
	Enable: IN std_logic;
	S: IN std_logic_vector(1 downto 0);
	OutDec: OUT std_logic_vector (3 downto 0)
  );
end Decoder2x4;

architecture Decoder2x4_ARCH of Decoder2x4 is
begin
	OutDec <= "0000" when Enable = '0'
	else "0001" when Enable = '1' and S = "00"
	else "0010" when Enable = '1' and S = "01"
	else "0100" when Enable = '1' and S = "10"
	else "1000";
	
end Decoder2x4_ARCH;