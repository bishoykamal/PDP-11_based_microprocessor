mem load -i {D:/college/Arch/project/final_project/New folder/Arch-final/initRom.mem} /final/cPart/RomPart/rom
mem load -i {D:/college/Arch/project/final_project/New folder/Arch-final/initRam.mem} /final/ramPart/ram

force -freeze sim:/final/Clk 0 0, 1 {50 ps} -r 100
force -freeze sim:/final/Rst 1 0
run
force -freeze sim:/final/Rst 0 0
run

