library ieee;
use ieee.std_logic_1164.all;
entity busAndRegs is
Port(
EnableReg:In std_logic_vector (11 downto 0); -- we have 10 reg but pc is enabled with 2 circuits
EnableOutOfReg:In std_logic_vector (9 downto 0); 
Clk,Reset,memoryflag:IN std_logic ;
outMemory:IN std_logic_vector(31 downto 0);
outOfMar: out std_logic_vector (31 downto 0);
AluSelection : in std_logic_vector(4 downto 0);
outputFlag: out std_logic_vector (1 downto 0);
outOfMdr:out std_logic_vector (31 downto 0);
outOfIR:out std_logic_vector (31 downto 0);
 BusB,BusC : Inout std_logic_vector (31 downto 0);
 flagEnable:in std_logic ;
valid : in std_logic
);
END entity busAndRegs;

ARCHITECTURE busAndRegs OF busAndRegs IS

component DFlipFlop is
GENERIC (n : integer := 32);
PORT( Clk,Rst,EnableDFF  : IN std_logic;
d : IN std_logic_vector(n-1 DOWNTO 0);
 q : OUT std_logic_vector(n-1 DOWNTO 0)
);
END COMPONENT;

component myTristate is 
GENERIC (n : integer := 32);
port (myIn:in std_logic_vector (n-1 downto 0);
control:in std_logic;
myOut: out std_logic_vector (n-1 downto 0)
);
END COMPONENT;

component mux2 is 
GENERIC (n : integer := 1);
PORT (SEl	:  IN std_logic;
       IN1,IN2 :IN std_logic_vector (31 downto 0);
       OUT1        : OUT  std_logic_vector (31 downto 0));  
END COMPONENT;

component AluProject is 
port(Src,BusB : in std_logic_vector (31 downto 0);
Clk,Reset : in std_logic ;
selection : in std_logic_vector(4 downto 0);
outputFlag: out std_logic_vector (1 downto 0);
flagEnable:in std_logic ;
F: out std_logic_vector (31 downto 0)
);
END COMPONENT ;

SIGNAL q0,q1,q2,q3,qMdr,qPc,qSp,qDst: std_logic_vector(31 downto 0);
SIGNAL qIr ,qIr2: std_logic_vector(31 downto 0);
SIGNAL outMux ,outOfSrc: std_logic_vector (31 downto 0);
SIGNAL temp,temp1,temp2,notClk : std_logic ;
SIGNAL outputFlagSignal: std_logic_vector (1 downto 0);
SIGNAL outAlu : std_logic_vector (31 downto 0);
SIGNAL outOfIrToBus:std_logic_vector (31 downto 0);

BEGIN

--process(Clk)
  --begin
    temp1<= (EnableReg(4) OR memoryflag);
 ---- end process;

ALU:AluProject PORT MAP (outOfSrc,BusB,Clk,Reset,AluSelection,outputFlagSignal,flagEnable,outAlu);
outputFlag<=outputFlagSignal;
BusC<=outAlu;
notClk <= not Clk;
R0: DFlipFlop GENERIC MAP(n=>32) PORT MAP (Clk,Reset,EnableReg(0),BusC,q0);
R1: DFlipFlop GENERIC MAP(n=>32)PORT MAP (Clk,Reset,EnableReg(1),BusC,q1);
R2: DFlipFlop GENERIC MAP(n=>32)PORT MAP (Clk,Reset,EnableReg(2),BusC,q2);
R3: DFlipFlop GENERIC MAP(n=>32)PORT MAP (Clk,Reset,EnableReg(3),BusC,q3);


mux:mux2 GENERIC MAP(n=>32)PORT MAP (memoryflag,BusC,outMemory,outMux);
  
Mdr:DFlipFlop GENERIC MAP(n=>32)PORT MAP (notClk,Reset,temp1,outMux,qMdr);
  
outOfMdr<=qMdr;
Sp:DFlipFlop GENERIC MAP(n=>32)PORT MAP  (Clk,Reset,EnableReg(5),BusC,qSp);
Dest:DFlipFlop GENERIC MAP(n=>32)PORT MAP(Clk,Reset,EnableReg(6),BusC,qDst);

Ir:DFlipFlop GENERIC MAP(n=>32)PORT MAP (Clk,Reset,EnableReg(7),BusC,qIr);--ssss
outOfIr<=qIr;
temp<=EnableReg(8) OR EnableReg(9);
Pc:DFlipFlop GENERIC MAP(n=>32) PORT MAP (Clk,Reset,temp,BusC,qPc);
Mar:DFlipFlop GENERIC MAP(n=>32) PORT MAP (Clk,Reset,EnableReg(10),BusC,outOfMar);
Src:DFlipFlop GENERIC MAP(n=>32) PORT MAP (Clk,Reset,EnableReg(11),BusC,outOfSrc);

T0:myTristate GENERIC MAP (n=>32) PORT MAP (q0,EnableOutOfReg(0),BusB);
T1:myTristate GENERIC MAP (n=>32) PORT MAP (q1,EnableOutOfReg(1),BusB);
T2:myTristate GENERIC MAP (n=>32) PORT MAP (q2,EnableOutOfReg(2),BusB);
T3:myTristate GENERIC MAP (n=>32) PORT MAP (q3,EnableOutOfReg(3),BusB);
TMdr:myTristate GENERIC MAP (n=>32) PORT MAP (qMdr,EnableOutOfReg(4),BusB);
TSp:myTristate GENERIC MAP (n=>32) PORT MAP (qSp,EnableOutOfReg(5),BusB);
TDest:myTristate GENERIC MAP (n=>32) PORT MAP (qDst,EnableOutOfReg(6),BusB);


qIr2<=qIr when valid='0'
else ("00000000000000000000011111111111" and qIr);

TIr:myTristate GENERIC MAP (n=>32) PORT MAP (qIr2,EnableOutOfReg(7),outOfIrToBus);
BusB<=outOfIrToBus;
TPc:myTristate GENERIC MAP (n=>32) PORT MAP (qPc,temp2,BusB);
temp2<=EnableOutOfReg(9) OR  EnableOutOfReg(8);



END busAndRegs;