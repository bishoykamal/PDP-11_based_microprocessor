library ieee;
use ieee.std_logic_1164.all;
ENTITY DFlipFlopOneBit IS
PORT( Clk,Rst,EnableDFF  : IN std_logic;
d : IN std_logic;
 q : OUT std_logic
);
END DFlipFlopOneBit;


ARCHITECTURE DFlipFlopOneBitA OF DFlipFlopOneBit IS
BEGIN
PROCESS (Clk,Rst)
BEGIN
IF rising_edge(Clk) AND Rst = '1' THEN
		q <= '0';
ELSIF (falling_edge(Clk) AND EnableDFF = '1')THEN
		q <= d;
END IF;
END PROCESS;
END DFlipFlopOneBitA;


