LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY PartC IS
	PORT(
	  Clk : IN std_logic;
	  Rst : IN std_logic;
		IRin    : IN  std_logic_vector(31 DOWNTO 0);
		Z,C: IN std_logic;
		keepValidValue: OUT std_logic;
		cntlWord : OUT std_logic_vector(22 DOWNTO 0);
		outLastDecoder:  OUT std_logic_vector (3 downto 0));	
END ENTITY PartC;

ARCHITECTURE PartCArch OF PartC IS
  
-- import 5 PLAs
  COMPONENT Pla_op IS
	PORT(
		input : IN  std_logic_vector(4 DOWNTO 0);
		IRin : IN  std_logic_vector(31 DOWNTO 0);
		enable : IN  std_logic;
		
		output : OUT std_logic_vector(6 DOWNTO 0));
  END COMPONENT;
  
  COMPONENT Pla_Src IS
	PORT(
	  
		IRin    : IN  std_logic_vector(31 DOWNTO 0);
		enable : IN  std_logic;
		output : OUT std_logic_vector(6 DOWNTO 0));
		
  END COMPONENT;
  
  COMPONENT Pla_Dst IS
	PORT(
	  
		IRin    : IN  std_logic_vector(31 DOWNTO 0);
		enable : IN  std_logic;
		output : OUT std_logic_vector(6 DOWNTO 0));
		
  END COMPONENT;
  
  COMPONENT Pla_Dst_Address IS
	PORT(
	  
		IRin    : IN  std_logic_vector(31 DOWNTO 0);
		enable : IN  std_logic;
		output : OUT std_logic_vector(6 DOWNTO 0));
		
  END COMPONENT;
  
  COMPONENT Pla_Write IS
	PORT(
	  
		IRin    : IN  std_logic_vector(31 DOWNTO 0);
		enable : IN  std_logic;
		output : OUT std_logic_vector(6 DOWNTO 0));
		
  END COMPONENT;
  
  
  --import D-flipflop & incrementer
  COMPONENT Incrementer IS
    GENERIC ( n : integer := 7);
    PORT( Clk,Rst,EnableDFF  : IN std_logic;
          d : IN std_logic_vector(n-1 DOWNTO 0);
          q : OUT std_logic_vector(n-1 DOWNTO 0)
        );
  END COMPONENT;
  
  COMPONENT DFlipFlop IS
    GENERIC ( n : integer := 7);
    PORT( Clk,Rst,EnableDFF  : IN std_logic;
          d : IN std_logic_vector(n-1 DOWNTO 0);
          q : OUT std_logic_vector(n-1 DOWNTO 0)
        );
  END COMPONENT;
  
  --import mux & control
  
  COMPONENT MUX4 IS
	 generic(n:integer:=7);
     PORT(in1,in2,in3,in4: IN std_logic_vector(n-1 downto 0);
	  sel: IN std_logic_vector(1 downto 0);
	  enable: IN std_logic;
	  output: OUT std_logic_vector(n-1 downto 0));
  END COMPONENT;
  
  COMPONENT cntlMux is
    port(PLA: in std_logic_vector(3 downto 0) ;
        PLAEnb : in std_logic;
        x,y,xy: out std_logic
    );

  end COMPONENT ;
  
  -- ROM
  
  COMPONENT ROM IS
	PORT(
		address : IN  std_logic_vector(6 DOWNTO 0);
		dataout : OUT std_logic_vector(22 DOWNTO 0));
  END COMPONENT;
  
  COMPONENT encoder_4_2 IS
     PORT(
	  input: IN std_logic_vector(3 downto 0);
	  output: OUT std_logic_vector(1 downto 0));
  END COMPONENT;
    
    
    -- Decoding Part
    
  COMPONENT decodingPart is 
  port(
	IRin: IN std_logic_vector (31 downto 0);
	IRen: IN std_logic;
	Z,C: IN std_logic;
	val: OUT std_logic;
	OutEncoder: OUT std_logic_vector (4 downto 0);
	outLastDecoder:  OUT std_logic_vector (3 downto 0)
  );
  end COMPONENT;
  
  COMPONENT DFlipFlopOneBit IS
  PORT( Clk,Rst,EnableDFF  : IN std_logic;
  d : IN std_logic;
  q : OUT std_logic
    );
  END COMPONENT;

  
  SIGNAL ControlWord : std_logic_vector(22 downto 0);
  SIGNAL PlaMainInput : std_logic_vector(4 downto 0);
  SIGNAL PlaMuxSelector,MainMuxSelector : std_logic_vector(1 downto 0);
  SIGNAL returnRegENB,MainPlaEnable,MainMuxEnable,upcReset,keepValid,ValidBranch,RstValidBranch,notReset,notClock,upcReset2: std_logic;
  SIGNAL Pla0Out,Pla1Out,Pla2Out,Pla3Out,Pla4Out,PlaMuxOut,MainMuxOut,upcOut,returnRegOut,incrementRegOut : std_logic_vector(6 downto 0);
	
	BEGIN
  process(Clk) is
  begin
    if(ControlWord(19 downto 17) = "101")  then
      MainPlaEnable <=   '1';
    else
      MainPlaEnable <=   '0';      
    end if;
  
      
      
      
      
      if falling_edge(Clk) then
      upcReset<= (ControlWord(1)) ;
      else
      upcReset<= '0' ;
      end if;
      
   
      
end process;


  upcReset2<=upcReset or Rst;
  cntlWord <= ControlWord;
  RstValidBranch <= ControlWord(1) OR Rst;
  keepValid <= '1' when ValidBranch='1';
  notClock <= not Clk;
  notReset <= not upcReset2;
  
  dPart    : decodingPart PORT MAP (IRin,MainPlaEnable,Z,C,ValidBranch,PlaMainInput,outLastDecoder);
  
  upc       : DFlipFlop   PORT MAP (notClock,upcReset2,notReset,MainMuxOut,upcOut);

  
  keep      : DFlipFlopOneBit  PORT MAP (Clk,RstValidBranch,ValidBranch,ValidBranch,keepValidValue);
  	
  returnReg : DFlipFlop   PORT MAP (notClock,Rst,returnRegENB,incrementRegOut,returnRegOut);
  
  incReg    : Incrementer PORT MAP (Clk,Rst,'1',upcOut,incrementRegOut);
    
  Pla0      : Pla_op PORT MAP (PlaMainInput,IRin,MainPlaEnable,Pla0Out);
    
  Pla1      : Pla_Src PORT MAP (IRin,ControlWord(5),Pla1Out);
  Pla2      : Pla_Dst PORT MAP (IRin,ControlWord(4),Pla2Out);
  Pla3      : Pla_Dst_Address PORT MAP (IRin,ControlWord(3),Pla3Out);
  Pla4      : Pla_Write PORT MAP (IRin,ControlWord(2),Pla4Out);
  
  muxPla     : MUX4 PORT MAP (Pla1Out,Pla2Out,Pla3Out,Pla4Out,PlaMuxSelector,'1',PlaMuxOut);
  muxMain    : MUX4 PORT MAP (returnRegOut,Pla0Out,incrementRegOut,PlaMuxOut,MainMuxSelector,'1',MainMuxOut);
  
  
  cntlMainMux: cntlMux PORT MAP (ControlWord(5 downto 2),MainPlaEnable,MainMuxSelector(1),MainMuxSelector(0),returnRegENB);
  encoder    : encoder_4_2 PORT MAP (ControlWord(5 downto 2),PlaMuxSelector);
    
  RomPart        : ROM PORT MAP (upcOut,ControlWord);

END PartCArch;




