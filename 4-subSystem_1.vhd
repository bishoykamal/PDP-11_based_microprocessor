
library ieee;
use ieee.std_logic_1164.all;

entity decodingPart is 
  port(
	IRin: IN std_logic_vector (31 downto 0);
	IRen: IN std_logic;
	Z,C: IN std_logic;
	val: OUT std_logic;
	OutEncoder: OUT std_logic_vector (4 downto 0);
	outLastDecoder:  OUT std_logic_vector (3 downto 0)
  );
end decodingPart;

architecture decodingPartArch of decodingPart is
	
	component Decoder2x4 is
	  port (
		Enable: IN std_logic;
		S: IN std_logic_vector(1 downto 0);
		OutDec: OUT std_logic_vector (3 downto 0)
	  );
	end component;

	component Decoder3x8 is
	  port (
		Enable: IN std_logic;
		S: IN std_logic_vector(2 downto 0);
		OutDec: OUT std_logic_vector (7 downto 0)
	  );
	end component;

	component Decoder4x16 is
	  port (
		Enable: IN std_logic;
		S: IN std_logic_vector(3 downto 0);
		OutDec: OUT std_logic_vector (15 downto 0)
	  );
	end component;

	component Encoder32x5 is
	  port (
		INbits: IN std_logic_vector(31 downto 0);
		OUTbits: OUT std_logic_vector (4 downto 0)
	  );
	end component;
	
	--signals =====================
	signal Dec0Out : std_logic_vector (3 downto 0);
	signal Dec1Out: std_logic_vector (15 downto 0);
	signal Dec2Out: std_logic_vector (15 downto 0);
	signal Dec3Out : std_logic_vector (7 downto 0);
	signal Dec4Out: std_logic_vector (3 downto 0);
	signal EncoderBit0 : std_logic;
	signal bigOR1 : std_logic;	
	signal bigOR2 : std_logic;
	signal EncoderInputHelper : std_logic_vector (31 downto 0);
	--=============================
	
begin
	--Decoders Layer
	Dec0: Decoder2x4  port map (IRen, IRin(15 downto 14), Dec0Out);
	--so2al : hena hadi ani decoder anhi bit ? 0 1 2 3 ?? eh el tartib y3ni 	
	--y3ni da hy5od most sig. wla least sig. ??
	Dec1: Decoder4x16 port map (Dec0Out(0), IRin(13 downto 10), Dec1Out);
	Dec2: Decoder4x16 port map (Dec0Out(1), IRin(13 downto 10), Dec2Out);
	Dec3: Decoder3x8  port map (Dec0Out(2), IRin(13 downto 11), Dec3Out);
	Dec4: Decoder2x4  port map (Dec0Out(3), IRin(13 downto 12), Dec4Out);
	--=======
	outLastDecoder <= Dec4Out;
	--Logic Layer (decoder 3)
	bigOR1 <= Dec3Out(7) or Dec3Out(6) or Dec3Out(5) or Dec3Out(4) or Dec3Out(3) or Dec3Out(2) or Dec3Out(1) or Dec3Out(0);
	--so2al: hna bardo l tartib min most sig. we min least sig.??
	bigOR2 <= (Dec3Out(0)) or
		  (Dec3Out(1) and Z) or 
		  (Dec3Out(2) and (not Z)) or
		  (Dec3Out(3) and (not C)) or 
		  (Dec3Out(4) and (Z or (not C))) or
		  (Dec3Out(5) and C) or
		  (Dec3Out(6) and (C or Z));	
	val <= (Dec3Out(0)) or
		  (Dec3Out(1) and Z) or 
		  (Dec3Out(2) and (not Z)) or
		  (Dec3Out(3) and (not C)) or 
		  (Dec3Out(4) and (Z or (not C))) or
		  (Dec3Out(5) and C) or
		  (Dec3Out(6) and (C or Z));
		  
	EncoderBit0 <= bigOr1 and (not bigOR2);
	--======
	--Encoder input
	EncoderInputHelper <= "00000" & Dec4Out(3) & Dec4Out(1) & Dec3Out(7) & bigOR2 & Dec2Out(10 downto 0) & Dec1Out(10 downto 0) & EncoderBit0;
	Enc0: Encoder32x5 port map (EncoderInputHelper, OutEncoder);

end decodingPartArch;


