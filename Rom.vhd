LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.numeric_std.all;

ENTITY ROM IS
	PORT(
		address : IN  std_logic_vector(6 DOWNTO 0);
		dataout : OUT std_logic_vector(22 DOWNTO 0));
END ENTITY ROM;

ARCHITECTURE romArch OF ROM IS

	TYPE rom_type IS ARRAY(0 TO 125) OF std_logic_vector(22 DOWNTO 0);
	SIGNAL rom : rom_type;
	
	BEGIN
		
	dataout <= rom(to_integer(unsigned(address)));
END romArch;
