LIBRARY IEEE;
USE IEEE.std_logic_1164.all;

ENTITY mux2 IS  
GENERIC (n : integer := 1);
PORT (SEl	:  IN std_logic;
       IN1,IN2 :IN std_logic_vector (n-1 downto 0);
       OUT1: OUT  std_logic_vector (n-1 downto 0)
);   
END ENTITY mux2;


ARCHITECTURE Data_flow OF mux2 IS
BEGIN
     
   OUT1 <= IN1 when  SEL='0'
          ELSE IN2;

     
END Data_flow;
