LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.numeric_std.all;

ENTITY Pla_Src IS
	PORT(
	  
		IRin    : IN  std_logic_vector(31 DOWNTO 0);
		enable : IN  std_logic;
		output : OUT std_logic_vector(6 DOWNTO 0));
		
END ENTITY Pla_Src;

ARCHITECTURE Pla_SrcArch OF Pla_Src IS

	BEGIN
		
		
		output <=   std_logic_vector(to_unsigned(3,7))  WHEN IRin(6 downto 5)="00"  AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(4,7))  WHEN IRin(6 downto 5)="01"  AND enable ='1' ELSE 
              		std_logic_vector(to_unsigned(7,7))  WHEN IRin(6 downto 5)="10"  AND enable ='1' ELSE 
              		std_logic_vector(to_unsigned(9,7))  WHEN IRin(6 downto 5)="11"  AND enable ='1' ELSE
            		
              		
		            (others =>'Z');
	
	
END Pla_SrcArch;


