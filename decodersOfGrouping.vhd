LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
ENTITY decodersOfGrouping IS
port( SpIn,SpOut ,Ir9to7,Ir4to2: in std_logic_vector(2 downto 0) ;
Ir15to14: in std_logic_vector(1 downto 0) ;
SpInOut,SpOutOut,RiInOut,RiOutOut :out std_logic_vector (7 downto 0);
Rin,Rout: in std_logic;
inTempReg: in std_logic_vector(0 downto 0)
);
END decodersOfGrouping;
ARCHITECTURE decodersOfGrouping OF decodersOfGrouping IS
component Decoder is 
port (
selection : IN std_logic_vector(2 downto 0);
enableDecoder : IN std_logic;
output : out std_logic_vector(7 downto 0)
);
END COMPONENT;

component mux2 is 
GENERIC (n : integer := 1);
PORT (SEl	:  IN std_logic;
       IN1,IN2 :IN std_logic_vector (n-1 downto 0);
       OUT1        : OUT  std_logic_vector (n-1 downto 0));  
END COMPONENT;


SIGNAL SpInOutput,SpOutOutput,RiInOutput,RiOutOutput: std_logic_vector(7 downto 0);
SIGNAL outMux : std_logic_vector (2 downto 0);
SIGNAL outTempReg : std_logic_vector (0 downto 0);
SIGNAL outTempReg2 :std_logic;
begin 
  
spInFlag:Decoder PORT MAP(SpIn,'1',SpInOutput);
SpInOut<=SpInOutput;
spOutFlag:Decoder PORT MAP(SpOut,'1',SpOutOutput);
SpOutOut<=SpOutOutput;

outTempReg2<= '0' when Ir15to14="01" else inTempReg(0);


mux:mux2 GENERIC MAP(n=>3)PORT MAP (outTempReg2,Ir9to7,Ir4to2,outMux);
riIn:Decoder PORT MAP(outMux,Rin,RiInOutput);
riOut:Decoder PORT MAP(outMux,Rout,RiOutOutput);
  
RiInOut<=RiInOutput;
RiOutOut<=RiOutOutput;
END decodersOfGrouping;
