LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
ENTITY encoder_4_2 IS
     PORT(
	  input: IN std_logic_vector(3 downto 0);
	  output: OUT std_logic_vector(1 downto 0));
END encoder_4_2;

ARCHITECTURE encoder_4_2arch OF encoder_4_2 IS
BEGIN
output <= "00" WHEN input="1000" ELSE
	  "01" when input="0100" Else
	  "10" when input="0010" ELse
	  "11" when input="0001" else
	  "00";
END encoder_4_2arch;
