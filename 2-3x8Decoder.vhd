
library ieee;
use ieee.std_logic_1164.all;

entity Decoder3x8 is
  port (
	Enable: IN std_logic;
	S: IN std_logic_vector(2 downto 0);
	OutDec: OUT std_logic_vector (7 downto 0)
  );
end Decoder3x8;

architecture Decoder3x8_ARCH of Decoder3x8 is
begin
	OutDec <= "00000000" when Enable = '0'
	else "00000001" when Enable = '1' and S = "000"
	else "00000010" when Enable = '1' and S = "001"
	else "00000100" when Enable = '1' and S = "010"
	else "00001000" when Enable = '1' and S = "011"
	else "00010000" when Enable = '1' and S = "100"
	else "00100000" when Enable = '1' and S = "101"
	else "01000000" when Enable = '1' and S = "110"
	else "10000000";
	
end Decoder3x8_ARCH;