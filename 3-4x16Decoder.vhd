

library ieee;
use ieee.std_logic_1164.all;

entity Decoder4x16 is
  port (
	Enable: IN std_logic;
	S: IN std_logic_vector(3 downto 0);
	OutDec: OUT std_logic_vector (15 downto 0)
  );
end Decoder4x16;

architecture Decoder4x16_ARCH of Decoder4x16 is
begin
	OutDec <= "0000000000000000" when Enable = '0'
	else "0000000000000001" when Enable = '1' and S = "0000"
	else "0000000000000010" when Enable = '1' and S = "0001"
	else "0000000000000100" when Enable = '1' and S = "0010"
	else "0000000000001000" when Enable = '1' and S = "0011"
	else "0000000000010000" when Enable = '1' and S = "0100"
	else "0000000000100000" when Enable = '1' and S = "0101"
	else "0000000001000000" when Enable = '1' and S = "0110"
	else "0000000010000000" when Enable = '1' and S = "0111"
	else "0000000100000000" when Enable = '1' and S = "1000"	
	else "0000001000000000" when Enable = '1' and S = "1001"	
	else "0000010000000000" when Enable = '1' and S = "1010"
	else "0000100000000000" when Enable = '1' and S = "1011"
	else "0001000000000000" when Enable = '1' and S = "1100"
	else "0010000000000000" when Enable = '1' and S = "1101"
	else "0100000000000000" when Enable = '1' and S = "1110"
	else "1000000000000000";
	
end Decoder4x16_ARCH;