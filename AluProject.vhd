library ieee;
use ieee.std_logic_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
entity AluProject is
port(Src,BusB : in std_logic_vector (31 downto 0);
Clk,Reset : in std_logic ;
selection : in std_logic_vector(4 downto 0);
outputFlag: out std_logic_vector (1 downto 0);
flagEnable:in std_logic ;
F: out std_logic_vector (31 downto 0)
);

end entity AluProject ;

Architecture AluProject of AluProject is 

component DFlipFlop is 
GENERIC ( n : integer := 32);
PORT( Clk,Rst,EnableDFF  : IN std_logic;
d : IN std_logic_vector(n-1 DOWNTO 0);
 q : OUT std_logic_vector(n-1 DOWNTO 0)
);
END COMPONENT;


component my_nadder is 
GENERIC(n:integer:=16);
PORT(a,b : IN std_logic_vector(n-1  DOWNTO 0);
            cin : IN std_logic;  
            s : OUT std_logic_vector(n-1 DOWNTO 0);    
            cout : OUT std_logic);
END COMPONENT;

SIGNAL op1,op2 ,outAdder,tempF : std_logic_vector (31 downto 0);
SIGNAL outFlag ,calcFlag: std_logic_vector(1 downto 0);
SIGNAL carryOut ,carryIn,EnableReg: std_logic ;
SIGNAL myone : std_logic_vector(31 downto 0);
SIGNAL myTwo : std_logic_vector(31 downto 0);
begin 

myone <= (0=>'1',others =>'0');
myTwo <= (1=>'1',others =>'0');

Flag: DFlipFlop GENERIC MAP(n=>2) PORT MAP (Clk,Reset,flagEnable,calcFlag,outFlag);
u0: my_nadder GENERIC MAP (n=>32) PORT MAP (op1,op2,carryIn,outAdder,carryout);

EnableReg<= '0' when selection(0)='0' AND selection(1)='0'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0'
else '1';
  
outputFlag<=outFlag;

calcFlag(1)<=carryout when (selection(0)='1' AND selection(1)='0'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0') OR
(selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0')OR 
(selection(0)='1' AND selection(1)='1'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0')OR
(selection(0)='0' AND selection(1)='0'AND selection(2)='1'AND selection(3)='0' AND selection(4)='0' )OR
(selection(0)='1' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0')OR
(selection(0)='0' AND selection(1)='0'AND selection(2)='1'AND selection(3)='1' AND selection(4)='0')OR
(selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0') OR
(selection(0)='0' AND selection(1)='0'AND selection(2)='0'AND selection(3)='1' AND selection(4)='1') OR
 (selection(0)='1' AND selection(1)='0'AND selection(2)='0'AND selection(3)='1' AND selection(4)='1') OR
 (selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='1')OR
 (selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0')
else Src(0) When selection(0)='1' AND selection(1)='0'AND selection(2)='0'AND selection(3)='0' AND selection(4)='1'
else Src(31) when  selection(0)='1' AND selection(1)='0'AND selection(2)='1'AND selection(3)='0' AND selection(4)='1'
else outFlag(1);

--op1 is Reg Src  
op1 <= Src when selection(0)='1' AND selection(1)='0'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0' --Src reg
else Src  when selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0'
else (not Src) when  selection(0)='1' AND selection(1)='1'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0'
else (not (Src + outFlag(1))) when selection(0)='0' AND selection(1)='0'AND selection(2)='1'AND selection(3)='0' AND selection(4)='0'
else Src when selection(0)='1' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0'
else Src when selection(0)='0' AND selection(1)='0'AND selection(2)='1'AND selection(3)='1' AND selection(4)='0'
else (not Src) when selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0'
else BusB when selection(0)='0' AND selection(1)='0'AND selection(2)='0'AND selection(3)='1' AND selection(4)='1'
else BusB when selection(0)='1' AND selection(1)='0'AND selection(2)='0'AND selection(3)='1' AND selection(4)='1'
else BusB when  selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='1'
else Src when selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0';


--op2 is BusB w howa Dest f nfs w2t 
op2 <= BusB when selection(0)='1' AND selection(1)='0'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0'
else BusB when selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0'
else BusB when selection(0)='1' AND selection(1)='1'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0'
else BusB when selection(0)='0' AND selection(1)='0'AND selection(2)='1'AND selection(3)='0' AND selection(4)='0'
else (myone) when selection(0)='1' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0'
else (not myone)when selection(0)='0' AND selection(1)='0'AND selection(2)='1'AND selection(3)='1' AND selection(4)='0'
else BusB when selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0'
else (myone) when selection(0)='0' AND selection(1)='0'AND selection(2)='0'AND selection(3)='1' AND selection(4)='1'
else myTwo when selection(0)='1' AND selection(1)='0'AND selection(2)='0'AND selection(3)='1' AND selection(4)='1'
else not myTwo when selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='1'
else not BusB when selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0';

-- carry in 
carryIn <= '0' when selection(0)='1' AND selection(1)='0'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0'
else outFlag(1) when selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0'
else '1' when selection(0)='1' AND selection(1)='1'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0'
else ('1') when selection(0)='0' AND selection(1)='0'AND selection(2)='1'AND selection(3)='0' AND selection(4)='0'
else '0' when selection(0)='1' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0'
else '1' when selection(0)='0' AND selection(1)='0'AND selection(2)='1'AND selection(3)='1' AND selection(4)='0'
else '1' when selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0'
else '0' when selection(0)='0' AND selection(1)='0'AND selection(2)='0'AND selection(3)='1' AND selection(4)='1'
else '0' when selection(0)='1' AND selection(1)='0'AND selection(2)='0'AND selection(3)='1' AND selection(4)='1'
else '1' when selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='1'
else '1' when selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0'; 


tempF<= (others =>'Z') when selection(0)='0' AND selection(1)='0'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0'
else (outAdder) when  selection(0)='1' AND selection(1)='0'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0'
else  (outAdder) when selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0'
else (outAdder) When selection(0)='1' AND selection(1)='1'AND selection(2)='0'AND selection(3)='0' AND selection(4)='0'
else (outAdder) When selection(0)='0' AND selection(1)='0'AND selection(2)='1'AND selection(3)='0' AND selection(4)='0'
else (Src AND BusB) When selection(0)='1' AND selection(1)='0'AND selection(2)='1'AND selection(3)='0' AND selection(4)='0'
else (Src OR BusB) When selection(0)='0' AND selection(1)='1'AND selection(2)='1'AND selection(3)='0' AND selection(4)='0'
else (Src XOR BusB) When selection(0)='1' AND selection(1)='1' AND selection(2)='1'AND selection(3)='0' AND selection(4)='0'
else (Src OR BusB) When selection(0)='0' AND selection(1)='0'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0'
else (BusB AND (not Src)) When selection(0)='1' AND selection(1)='0'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0'
else (outAdder) When selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0'
else (outAdder) When selection(0)='1' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='0'
else (outAdder) When selection(0)='0' AND selection(1)='0'AND selection(2)='1'AND selection(3)='1' AND selection(4)='0'
else (others=>'0') When selection(0)='1' AND selection(1)='0'AND selection(2)='1'AND selection(3)='1' AND selection(4)='0'
else (not Src) When selection(0)='0' AND selection(1)='1'AND selection(2)='1'AND selection(3)='1' AND selection(4)='0'
else ( '0' &  Src(31 downto 1)) When selection(0)='1' AND selection(1)='1'AND selection(2)='1'AND selection(3)='1' AND selection(4)='0'
else ( Src(0) &  Src(31 downto 1)) When selection(0)='0' AND selection(1)='0'AND selection(2)='0'AND selection(3)='0' AND selection(4)='1'
else ( outFlag(1)& Src(31 downto 1)) When selection(0)='1' AND selection(1)='0'AND selection(2)='0'AND selection(3)='0' AND selection(4)='1'
else ( Src(31) &  Src(31 downto 1)) When selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='0' AND selection(4)='1'
else ( Src(30 downto 0) & '0' ) When selection(0)='1' AND selection(1)='1'AND selection(2)='0'AND selection(3)='0' AND selection(4)='1'
else (Src(30 downto 0) & Src(31)) When selection(0)='0' AND selection(1)='0'AND selection(2)='1'AND selection(3)='0' AND selection(4)='1'
else (Src(30 downto 0) & outFlag(1)) When selection(0)='1' AND selection(1)='0'AND selection(2)='1'AND selection(3)='0' AND selection(4)='1'
else (BusB) When selection(0)='0' AND selection(1)='1'AND selection(2)='1'AND selection(3)='0' AND selection(4)='1'
else (Src) When selection(0)='1' AND selection(1)='1'AND selection(2)='1'AND selection(3)='0' AND selection(4)='1'
else outAdder when selection(0)='0' AND selection(1)='0'AND selection(2)='0'AND selection(3)='1' AND selection(4)='1'
else outAdder when selection(0)='1' AND selection(1)='0'AND selection(2)='0'AND selection(3)='1' AND selection(4)='1'
else outAdder when selection(0)='0' AND selection(1)='1'AND selection(2)='0'AND selection(3)='1' AND selection(4)='1' ;

calcFlag(0)<= '1' when tempF = "00000000000000000000000000000000" AND not ((selection(0)='0' AND selection(1)='1'AND selection(2)='1'AND selection(3)='0' AND selection(4)='1') OR  (selection(0)='1' AND selection(1)='1'AND selection(2)='1'AND selection(3)='0' AND selection(4)='1'))
else '0';
  
  
  
F<=tempF;
end AluProject ;