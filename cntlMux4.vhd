library ieee;
use ieee.std_logic_1164.all;
entity cntlMux is
port(PLA: in std_logic_vector(3 downto 0) ;
PLAEnb : in std_logic;
x,y,xy: out std_logic
);

end entity cntlMux ;

Architecture cntlMuxArch of cntlMux is 
SIGNAL A1,A2,A3,A4,A5 ,tempX,tempY: std_logic;
begin 

A1<=PLA(0) OR PLA(1) OR PLA(2) OR PLA(3);
A3<=PLA(0) AND PLA(1) AND PLA(2) AND PLA(3);
A2<= (not A1) AND (not PLAEnb);
A4<=(not A3) AND (not PLAEnb);
tempX<= A2 OR A4;
x<= tempX;
A5<= A3 XOR A1;
tempY<= A5 OR (PLAEnb); --not PLA
y<=tempY;
xy<= tempX AND tempY;
end cntlMuxArch ;
