LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.numeric_std.all;

ENTITY Pla_Dst IS
	PORT(
	  
		IRin    : IN  std_logic_vector(31 DOWNTO 0);
		enable : IN  std_logic;
		output : OUT std_logic_vector(6 DOWNTO 0));
		
END ENTITY Pla_Dst;

ARCHITECTURE Pla_DstArch OF Pla_Dst IS

	BEGIN
		
		
		output <=   std_logic_vector(to_unsigned(14,7))  WHEN IRin(1 downto 0)="00"  AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(15,7))  WHEN IRin(1 downto 0)="01"  AND enable ='1' ELSE 
              		std_logic_vector(to_unsigned(18,7))  WHEN IRin(1 downto 0)="10"  AND enable ='1' ELSE 
              		std_logic_vector(to_unsigned(20,7))  WHEN IRin(1 downto 0)="11"  AND enable ='1' ELSE
            		
              		
		            (others =>'Z');
	
	
END Pla_DstArch;




