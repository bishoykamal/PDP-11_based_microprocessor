LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.numeric_std.all;

ENTITY Pla_Dst_Address IS
	PORT(
	  
		IRin    : IN  std_logic_vector(31 DOWNTO 0);
		enable : IN  std_logic;
		output : OUT std_logic_vector(6 DOWNTO 0));
		
END ENTITY Pla_Dst_Address;

ARCHITECTURE Pla_Dst_AddressArch OF Pla_Dst_Address IS

	BEGIN
		
		
		output <=   std_logic_vector(to_unsigned(27,7))  WHEN IRin(1 downto 0)="01" AND IRin(15 downto 14)="00" AND enable ='1' ELSE 
              		std_logic_vector(to_unsigned(29,7))  WHEN IRin(1 downto 0)="10" AND IRin(15 downto 14)="00" AND enable ='1' ELSE 
              		std_logic_vector(to_unsigned(30,7))  WHEN IRin(1 downto 0)="11" AND IRin(15 downto 14)="00" AND enable ='1' ELSE
              		
              		
              		std_logic_vector(to_unsigned(27,7))  WHEN IRin(6 downto 5)="01" AND IRin(15 downto 14)="01" AND enable ='1' ELSE 
              		std_logic_vector(to_unsigned(29,7))  WHEN IRin(6 downto 5)="10" AND IRin(15 downto 14)="01" AND enable ='1' ELSE 
              		std_logic_vector(to_unsigned(30,7))  WHEN IRin(6 downto 5)="11" AND IRin(15 downto 14)="01" AND enable ='1' ELSE
              		
            		
              		
		            (others =>'Z');
	
	
END Pla_Dst_AddressArch;






