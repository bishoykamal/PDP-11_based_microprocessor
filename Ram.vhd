LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.numeric_std.all;
--if indicator=0 ->get 16
--if indicator =1 ->get 32
ENTITY Ram IS
	PORT(
		clk: in std_logic;
		address : IN  std_logic_vector(10 DOWNTO 0);
		data : inout std_logic_vector(31 DOWNTO 0);
		Rd,Wr:IN std_logic;
		indicator:IN std_logic);
END ENTITY Ram;

ARCHITECTURE RamArch OF Ram IS

	TYPE ram_type IS ARRAY(0 TO 2047) OF std_logic_vector(15 DOWNTO 0);
	SIGNAL ram : ram_type ;
	BEGIN	
	process(clk,data) is
	begin
	if rising_edge(clk) then
		if Wr='1' then
		   ram(to_integer(unsigned(address))) <= data(15 downto 0);
		   ram(to_integer(unsigned(address))+1) <= data(31 downto 16);
		end if;
	end if;	
	end process;
	data(31 downto 16) <=(others =>'0') when indicator='0' and Rd='1' ELSE	
			      ram(to_integer(unsigned(address))+1) when indicator='1' and Rd='1' else
				(others=>'Z');
	data(15 downto 0) <= ram(to_integer(unsigned(address))) when Rd='1' else
				(others=>'Z');
END RamArch;

