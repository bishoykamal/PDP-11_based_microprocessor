library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
ENTITY Incrementer IS
GENERIC ( n : integer := 3);
PORT( Clk,Rst,EnableDFF  : IN std_logic;
d : IN std_logic_vector(n-1 DOWNTO 0);
 q : OUT std_logic_vector(n-1 DOWNTO 0)
);
END Incrementer;


ARCHITECTURE IncrementerArch OF Incrementer IS
BEGIN
PROCESS (Clk,Rst)
BEGIN
IF Rst = '1' THEN
		q <= (OTHERS=>'0');
ELSIF (rising_edge(Clk) AND EnableDFF = '1')THEN
		q <= d+1;
END IF;
END PROCESS;
END IncrementerArch;
