LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
ENTITY MUX4 IS
	generic(n:integer:=32);
     PORT(in1,in2,in3,in4: IN std_logic_vector(n-1 downto 0);
	  sel: IN std_logic_vector(1 downto 0);
	  enable: IN std_logic;
	  output: OUT std_logic_vector(n-1 downto 0));
END MUX4;

ARCHITECTURE arch OF MUX4 IS
BEGIN
output <= in1 WHEN sel="00" and enable='1' ELSE
	  in2 WHEN sel="01" and enable='1' ELSE
          in3 WHEN sel="10" and enable='1' ELSE
	  in4 WHEN sel="11" and enable='1' ELSE
	  (others => 'Z');
END arch;
