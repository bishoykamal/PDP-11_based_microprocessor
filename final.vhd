LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.numeric_std.all;
--if indicator=0 ->get 16
--if indicator =1 ->get 32
ENTITY Final IS
	PORT(
		Clk: in std_logic;
		Rst:IN std_logic);
END ENTITY Final;



ARCHITECTURE FinalArch OF Final IS

COMPONENT PartC IS
	PORT(
	  Clk : IN std_logic;
	  Rst : IN std_logic;
		IRin    : IN  std_logic_vector(31 DOWNTO 0);
		Z,C: IN std_logic;
		keepValidValue: OUT std_logic;
		cntlWord : OUT std_logic_vector(22 DOWNTO 0);
		outLastDecoder:  OUT std_logic_vector (3 downto 0));
END COMPONENT;

COMPONENT Ram IS
	PORT(
		clk: in std_logic;
		address : IN  std_logic_vector(10 DOWNTO 0);
		data : inout std_logic_vector(31 DOWNTO 0);
		Rd,Wr:IN std_logic;
		indicator:IN std_logic);
END COMPONENT;


COMPONENT APART is
port(
SpIn,SpOut: in std_logic_vector(2 downto 0) ;
Rin,Rout: in std_logic;
inTempReg: in std_logic_vector(0 downto 0);
Clk,Reset,memoryflag:IN std_logic ;
outMemory:IN std_logic_vector(31 downto 0);
outOfMar: out std_logic_vector (31 downto 0);
AluSelection : in std_logic_vector(4 downto 0);
outputFlag: out std_logic_vector (1 downto 0);
valid : in std_logic;
outOfMdr:out std_logic_vector (31 downto 0);
flagEnable:in std_logic ;
outOfIR:out std_logic_vector (31 downto 0)
);

end COMPONENT ;


COMPONENT myTristate is
GENERIC (n : integer := 32);
port (myIn:in std_logic_vector (n-1 downto 0);
control:in std_logic;
myOut: out std_logic_vector (n-1 downto 0)
);
end COMPONENT;

SIGNAL ControlWord : std_logic_vector(22 downto 0);
SIGNAL ramAddress : std_logic_vector(31 downto 0);
SIGNAL ramDataFromRam,ramDataFromBus,ramBus : std_logic_vector(31 downto 0);
SIGNAL IR : std_logic_vector(31 downto 0);
SIGNAL lDecoder : std_logic_vector(3 downto 0);
SIGNAL inTempRegSig : std_logic_vector(0 downto 0);
SIGNAL regFlag : std_logic_vector(1 downto 0);
SIGNAL vBranch,flagENB,sysClk,sysRst : std_logic;

BEGIN
  
  sysClk <= not lDecoder(0) AND Clk;
  sysRst <= lDecoder(2) OR Rst;
  
  cPart    : PartC PORT MAP (sysClk,sysRst,IR,regFlag(0),regFlag(1),vBranch,ControlWord,lDecoder);
  ramPart    : Ram PORT MAP (sysClk,ramAddress(10 downto 0),ramDataFromRam,ControlWord(13),ControlWord(12),ControlWord(11));
    
  partAa     : APART PORT MAP( ControlWord(19 downto 17),ControlWord(16 downto 14),ControlWord(21),ControlWord(20),inTempRegSig,sysClk,sysRst,ControlWord(13),ramBus,ramAddress,ControlWord(10 downto 6),regFlag,vBranch,ramDataFromBus,ControlWord(22),IR );
  
  inTempRegSig(0)<= ControlWord(0);
  
  T1 : myTristate PORT MAP (ramDataFromBus,ControlWord(12),ramBus);
  T2 : myTristate PORT MAP (ramBus,ControlWord(12),ramDataFromRam);
  T3 : myTristate PORT MAP (ramDataFromRam,ControlWord(13),ramBus);
  
  
  
END FinalArch;