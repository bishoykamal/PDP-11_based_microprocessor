LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.numeric_std.all;

ENTITY Pla_op IS
	PORT(
		input : IN  std_logic_vector(4 DOWNTO 0);
		IRin : IN  std_logic_vector(31 DOWNTO 0);
		enable : IN  std_logic;
		
		output : OUT std_logic_vector(6 DOWNTO 0));
END ENTITY Pla_op;

ARCHITECTURE Pla_opArch OF Pla_op IS

	BEGIN
		-- it must use adressing mode bits
		
		output <=   std_logic_vector(to_unsigned(0,7)) WHEN input = std_logic_vector(to_unsigned(0,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(36,7)) WHEN input = std_logic_vector(to_unsigned(1,5)) AND enable ='1' AND IRin(1 downto 0)="00" ELSE -- need addressingMode
              		std_logic_vector(to_unsigned(38,7)) WHEN input = std_logic_vector(to_unsigned(1,5)) AND enable ='1' ELSE 
              		std_logic_vector(to_unsigned(46,7)) WHEN input = std_logic_vector(to_unsigned(2,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(50,7)) WHEN input = std_logic_vector(to_unsigned(3,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(54,7)) WHEN input = std_logic_vector(to_unsigned(4,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(58,7)) WHEN input = std_logic_vector(to_unsigned(5,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(62,7)) WHEN input = std_logic_vector(to_unsigned(6,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(66,7)) WHEN input = std_logic_vector(to_unsigned(7,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(70,7)) WHEN input = std_logic_vector(to_unsigned(8,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(74,7)) WHEN input = std_logic_vector(to_unsigned(9,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(78,7)) WHEN input = std_logic_vector(to_unsigned(10,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(82,7)) WHEN input = std_logic_vector(to_unsigned(11,5)) AND enable ='1' ELSE
              		
              		std_logic_vector(to_unsigned(85,7)) WHEN input = std_logic_vector(to_unsigned(12,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(88,7)) WHEN input = std_logic_vector(to_unsigned(13,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(42,7)) WHEN input = std_logic_vector(to_unsigned(14,5)) AND enable ='1' AND IRin(6 downto 5)="00" ELSE  -- need addressingMode
              		std_logic_vector(to_unsigned(43,7)) WHEN input = std_logic_vector(to_unsigned(14,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(91,7)) WHEN input = std_logic_vector(to_unsigned(15,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(94,7)) WHEN input = std_logic_vector(to_unsigned(16,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(97,7)) WHEN input = std_logic_vector(to_unsigned(17,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(100,7)) WHEN input = std_logic_vector(to_unsigned(18,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(103,7)) WHEN input = std_logic_vector(to_unsigned(19,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(106,7)) WHEN input = std_logic_vector(to_unsigned(20,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(109,7)) WHEN input = std_logic_vector(to_unsigned(21,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(112,7)) WHEN input = std_logic_vector(to_unsigned(22,5)) AND enable ='1' ELSE
              		
              		std_logic_vector(to_unsigned(115,7)) WHEN input = std_logic_vector(to_unsigned(23,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(121,7)) WHEN input = std_logic_vector(to_unsigned(24,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(117,7)) WHEN input = std_logic_vector(to_unsigned(25,5)) AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(126,7)) WHEN input = std_logic_vector(to_unsigned(26,5)) AND enable ='1' ELSE
              		
		            (others =>'Z');
	
	
END Pla_opArch;

