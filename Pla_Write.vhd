LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.numeric_std.all;

ENTITY Pla_Write IS
	PORT(
	  
		IRin    : IN  std_logic_vector(31 DOWNTO 0);
		enable : IN  std_logic;
		output : OUT std_logic_vector(6 DOWNTO 0));
		
END ENTITY Pla_Write;

ARCHITECTURE Pla_WriteArch OF Pla_Write IS

	BEGIN
		
		
		output <=   std_logic_vector(to_unsigned(118,7))  WHEN IRin(1 downto 0)="00" AND IRin(15 downto 14)="00" AND enable ='1' ELSE
		            std_logic_vector(to_unsigned(119,7))  WHEN IRin(15 downto 14)="00" AND enable ='1' ELSE 
              		
              		std_logic_vector(to_unsigned(118,7))  WHEN IRin(6 downto 5)="00" AND IRin(15 downto 14)="01" AND enable ='1' ELSE
              		std_logic_vector(to_unsigned(119,7))  WHEN IRin(15 downto 14)="01" AND enable ='1' ELSE 
              		         		
              		
		            (others =>'Z');
	
	
END Pla_WriteArch;



