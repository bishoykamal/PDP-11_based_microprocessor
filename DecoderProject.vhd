library ieee;
use ieee.std_logic_1164.all;
entity Decoder is
port (
selection : IN std_logic_vector(2 downto 0);
enableDecoder : IN std_logic;
output : out std_logic_vector(7 downto 0)
);
end entity Decoder;

Architecture  Decoder of Decoder is 
begin 
output <= (0=>'1' , OTHERS=>'0') when selection(0)='0' AND selection(1)='0'  AND selection(2)='0'AND enableDecoder='1'
          ELSE (1=>'1', OTHERS=>'0') WHEN  selection(0)='1' AND selection(1)='0'AND selection(2)='0' AND enableDecoder='1'
          ELSE (2=>'1', OTHERS=>'0') WHEN  selection(0)='0' AND selection(1)='1' AND selection(2)='0' AND enableDecoder='1'
          ELSE (3=>'1', OTHERS=>'0') WHEN  selection(0)='1' AND selection(1)='1' AND selection(2)='0' AND enableDecoder='1'
          ELSE (4=>'1', OTHERS=>'0') WHEN  selection(0)='0' AND selection(1)='0' AND selection(2)='1' AND enableDecoder='1'
          ELSE (5=>'1', OTHERS=>'0') WHEN  selection(0)='1' AND selection(1)='0' AND selection(2)='1' AND enableDecoder='1'
          ELSE (6=>'1',OTHERS=>'0')WHEN  selection(0)='0' AND selection(1)='1' AND selection(2)='1' AND enableDecoder='1'
          ELSE (7=>'1',OTHERS=>'0') WHEN  selection(0)='1' AND selection(1)='1' AND selection(2)='1' AND enableDecoder='1'
          ELSE ( OTHERS=>'0');
end Decoder ;
